package com.zwenex.sampleproject;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.zwenex.sampleproject.db.ApplicationDatabase;
import com.zwenex.sampleproject.db.BookEntity;
import com.zwenex.sampleproject.db.DataGenerator;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BookRecyclerViewAdapter.BookClickListener {

    private ArrayList<BookEntity> bookList = new ArrayList<>();
    private BookRecyclerViewAdapter adapter;
    private ApplicationDatabase appDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appDb = ApplicationDatabase.getInstance(this);

        RecyclerView bookRecyclerView = findViewById(R.id.book_recycler_view);
        bookRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new BookRecyclerViewAdapter();
        adapter.setBookClickListener(this);
        bookRecyclerView.setAdapter(adapter);

        LiveData<List<BookEntity>> bookListLiveData = appDb.bookDao().getAllBooks();

        bookListLiveData.observe(this, new Observer<List<BookEntity>>() {
            @Override
            public void onChanged(@Nullable List<BookEntity> bookEntities) {
                if(bookEntities.isEmpty()) {
                    appDb.bookDao().addBooks(DataGenerator.generateBooks());
                } else {
                    adapter.refreshList(bookEntities);
                }
            }
        });


    }

    @Override
    public void onBookClick(BookEntity book) {
        Intent intent = new Intent(this, BookDetailsActivity.class);
        intent.putExtra("bookId", book.getId());
        startActivity(intent);
    }
}

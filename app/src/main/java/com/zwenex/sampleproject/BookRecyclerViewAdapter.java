package com.zwenex.sampleproject;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zwenex.sampleproject.db.BookEntity;

import java.util.ArrayList;
import java.util.List;

public class BookRecyclerViewAdapter extends RecyclerView.Adapter<BookRecyclerViewAdapter.BookViewHolder> {

    private List<BookEntity> bookList;
    private BookClickListener clickListener;

    public void setBookClickListener(BookClickListener clickListener){
        this.clickListener = clickListener;
    }

    public void refreshList(List<BookEntity> bookList){
        this.bookList = bookList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final BookViewHolder holder, final int position) {
        holder.titleText.setText(bookList.get(position).getTitle());
        holder.authorText.setText(bookList.get(position).getAuthor());
        holder.pagesText.setText(String.valueOf(bookList.get(position).getPages()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onBookClick(bookList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (bookList!=null)
            return bookList.size();
        else
            return 0;
    }

    public static class BookViewHolder extends RecyclerView.ViewHolder {
        TextView titleText;
        TextView authorText;
        TextView pagesText;
        public BookViewHolder(View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.title_text);
            authorText = itemView.findViewById(R.id.author_text);
            pagesText = itemView.findViewById(R.id.pages_text);

        }
    }
    public interface BookClickListener{
        void onBookClick(BookEntity book);
    }

}
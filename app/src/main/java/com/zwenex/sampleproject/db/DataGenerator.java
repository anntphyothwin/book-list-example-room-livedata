package com.zwenex.sampleproject.db;

import java.util.ArrayList;
import java.util.List;

public class DataGenerator {
    public static List<BookEntity> generateBooks() {
        List<BookEntity> books = new ArrayList<>();

        books.add(new BookEntity("BookEntity A", "Author A", 50));
        books.add(new BookEntity("BookEntity B", "Author B", 51));
        books.add(new BookEntity("BookEntity C", "Author C", 52));
        books.add(new BookEntity("BookEntity D", "Author D", 53));
        return books;
    }
}

package com.zwenex.sampleproject.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;


/**
 * Main Database
 * The Database class must use Singleton design pattern and must only use a single instance in all classes.
 */
@Database(entities = {BookEntity.class}, version = 1)
public abstract class ApplicationDatabase extends RoomDatabase{
    private static ApplicationDatabase instance;

    /** Database name*/
    public static final String DATABASE_NAME = "example-db";

    /** All DAOs of the tables must be added here.*/
    public abstract BookDao bookDao();

    /** Checks if an instance already exists, and if not, creates a new instance and returns it.*/
    public static ApplicationDatabase getInstance(final Context context) {
        if(instance == null){
            instance = buildDatabase(context.getApplicationContext());
        }
        return instance;
    }

    private static ApplicationDatabase buildDatabase(final Context appContext) {
        return Room.databaseBuilder(appContext, ApplicationDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
    }
}

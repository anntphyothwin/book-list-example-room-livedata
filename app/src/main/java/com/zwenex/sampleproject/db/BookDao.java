package com.zwenex.sampleproject.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BookDao {
    @Query("SELECT * from books")
    LiveData<List<BookEntity>> getAllBooks();

    @Query("SELECT * from books where id = :bookId")
    LiveData<BookEntity> getBook(int bookId);

    /**
     * Using OnConflictStrategy.Replace replaces the record if a record with the same PRIMARY KEY exists
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addBook(BookEntity bookEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addBooks(List<BookEntity> bookEntities);

    @Query("DELETE from books")
    void deleteAllBooks();
}
package com.zwenex.sampleproject;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.zwenex.sampleproject.db.ApplicationDatabase;
import com.zwenex.sampleproject.db.BookEntity;

public class BookDetailsActivity extends AppCompatActivity {
    private ApplicationDatabase appDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        appDb = ApplicationDatabase.getInstance(this);

        final TextView bookTitle = findViewById(R.id.book_title);
        final TextView bookAuthor = findViewById(R.id.book_author);
        final TextView bookPages = findViewById(R.id.book_pages);

        int bookId = getIntent().getIntExtra("bookId",0);

        LiveData<BookEntity> bookLiveData = appDb.bookDao().getBook(bookId);
        bookLiveData.observe(this, new Observer<BookEntity>() {
            @Override
            public void onChanged(@Nullable BookEntity book) {
                bookTitle.setText(book.getTitle());
                bookAuthor.setText(book.getAuthor());
                bookPages.setText(String.valueOf(book.getPages()));
            }
        });

    }
}
